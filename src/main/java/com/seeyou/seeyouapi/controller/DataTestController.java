package com.seeyou.seeyouapi.controller;

import com.seeyou.seeyouapi.model.common.CommonResult;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.common.SingleResult;
import com.seeyou.seeyouapi.model.dataTest.*;
import com.seeyou.seeyouapi.service.DataTestService;
import com.seeyou.seeyouapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "로그인")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/data-test")
public class DataTestController {
    private final DataTestService dataTestService;

    @ApiOperation(value = "등록")
    @PostMapping("/new")
    public CommonResult setData(@RequestBody @Valid DataTestCreateRequest request) {
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "리스트")
    @GetMapping("/all")
    public ListResult<DataTestListItem> getList() {
        return ResponseService.getListResult(dataTestService.getList(), true);
    }

    @ApiOperation(value = "상세")
    @GetMapping("/{id}")
    public SingleResult<DataTestDetailResponse> getDetail(@PathVariable long id) {
        return ResponseService.getSingleResult(dataTestService.getDetail());
    }

    @ApiOperation(value = "댓글등록")
    @PostMapping("/comment/document-id/{id}")
    public CommonResult setComment(@PathVariable long id, @RequestBody @Valid DataTestCommentRequest commentRequest) {
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "수정")
    @PutMapping("/{id}")
    public CommonResult putData(@PathVariable long id, @RequestBody @Valid DataTestUpdateRequest updateRequest) {
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "삭제")
    @DeleteMapping("/{id}")
    public CommonResult delData(@PathVariable long id) {
        return ResponseService.getSuccessResult();
    }
}
