package com.seeyou.seeyouapi.entity;

import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import com.seeyou.seeyouapi.lib.CommonFormat;
import com.seeyou.seeyouapi.model.productorder.ProductOrderRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "productId", nullable = false)
    private Product product;

    @Column(nullable = false)
    private Integer quantity;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private String dateOrder;

    @Column
    private String completeDateOrder;

    @Column(nullable = false)
    private Boolean isComplete;

    public void putProductOrder() {
        this.isComplete = true;
        this.completeDateOrder = CommonFormat.convertLocalDateTimeToString(LocalDateTime.now());
    }

    private ProductOrder(Builder builder) {
        this.product = builder.product;
        this.quantity = builder.quantity;
        this.price = builder.price;
        this.dateOrder = builder.dateOrder;
        this.completeDateOrder = builder.completeDateOrder;
        this.isComplete = builder.isComplete;

    }

    public static class Builder implements CommonModelBuilder<ProductOrder> {
        private final Product product;
        private final Integer quantity;
        private final Double price;
        private final String dateOrder;
        private final String completeDateOrder;
        private final Boolean isComplete;

        public Builder(Product product, ProductOrderRequest request) {
            this.product = product;
            this.quantity = request.getQuantity();
            this.price = product.getUnitPrice() * quantity;
            this.dateOrder = CommonFormat.convertLocalDateTimeToString(LocalDateTime.now());
            this.completeDateOrder = null;
            this.isComplete = false;
        }
        @Override
        public ProductOrder build() {
            return new ProductOrder(this);
        }
    }
}
