package com.seeyou.seeyouapi.entity;

import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import com.seeyou.seeyouapi.lib.CommonFormat;
import com.seeyou.seeyouapi.model.sell.SellRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Sell {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Long billNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "productId", nullable = false)
    private Product product;

    @Column(nullable = false)
    private Integer quantity;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private String dateSell;

    private String dateRefund;

    @Column(nullable = false)
    private Boolean isComplete;

    public void putIsCompleteUpdate() {
        this.isComplete = false;
        this.dateRefund = CommonFormat.convertLocalDateTimeToString(LocalDateTime.now());
    }

    private Sell(Builder builder) {
        this.billNumber = builder.billNumber;
        this.product = builder.product;
        this.quantity = builder.quantity;
        this.price = builder.price;
        this.dateSell = builder.dateSell;
        this.dateRefund = builder.dateRefund;
        this.isComplete = builder.isComplete;
    }

    public static class Builder implements CommonModelBuilder<Sell> {
        private final Long billNumber;
        private final Product product;
        private final Integer quantity;
        private final Double price;
        private final String dateSell;
        private final String dateRefund;
        private final Boolean isComplete;

        public Builder(Product product, SellRequest sellRequest) {
            this.billNumber = sellRequest.getBillNumber();
            this.product = product;
            this.quantity = sellRequest.getQuantity();
            this.price = product.getUnitPrice() * quantity;
            this.dateSell = CommonFormat.convertLocalDateTimeToString(LocalDateTime.now());
            this.dateRefund = null;
            this.isComplete = true;
        }

        @Override
        public Sell build() {
            return new Sell(this);
        }
    }
}
