package com.seeyou.seeyouapi.model.attendance;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalTime;

@Getter
@Setter
public class AttendanceRequest {

    @ApiModelProperty(notes = "연도")
    @NotNull
    private Integer attendanceYear;

    @ApiModelProperty(notes = "월")
    @NotNull
    private Integer attendanceMonth;

    @ApiModelProperty(notes = "일")
    @NotNull
    private Integer attendanceDay;

    @ApiModelProperty(notes = "출근시간")
    private LocalTime timeAttendance;

    @ApiModelProperty(notes = "퇴근시간")
    private LocalTime timeLeaveWork;
}
