package com.seeyou.seeyouapi.model.productorder;

import com.seeyou.seeyouapi.entity.Product;
import com.seeyou.seeyouapi.entity.ProductOrder;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductOrderItem {
    // 상품명, 단가, 수량, 금액, 발주일

    @ApiModelProperty(notes = "발주 ID")
    private Long productOrderId;

    @ApiModelProperty(notes = "상품명")
    private String productName;

    @ApiModelProperty(notes = "수량")
    private Integer quantity;

    @ApiModelProperty(notes = "단가")
    private Double unitPrice;

    @ApiModelProperty(notes = "금액")
    private Double price;

    @ApiModelProperty(notes = "발주일")
    private String dateOrder;

    @ApiModelProperty(notes = "발주 상태")
    private Boolean isComplete;

    private ProductOrderItem (Builder builder) {
        this.productOrderId = builder.productOrderId;
        this.productName = builder.productName;
        this.quantity = builder.quantity;
        this.unitPrice = builder.unitPrice;
        this.price = builder.price;
        this.dateOrder = builder.dateOrder;
        this.isComplete = builder.isComplete;
    }

    public static class Builder implements CommonModelBuilder<ProductOrderItem> {
        private final Long productOrderId;
        private final String productName;
        private final Integer quantity;
        private final Double unitPrice;
        private final Double price;
        private final String dateOrder;
        private final Boolean isComplete;

        public Builder(ProductOrder productOrder) {
            this.productOrderId = productOrder.getId();
            this.productName = productOrder.getProduct().getProductName();
            this.quantity = productOrder.getQuantity();
            this.unitPrice = productOrder.getProduct().getUnitPrice();
            this.price = productOrder.getPrice();
            this.dateOrder = productOrder.getDateOrder();
            this.isComplete = productOrder.getIsComplete();
        }
        @Override
        public ProductOrderItem build() {
            return new ProductOrderItem(this);
        }
    }
}
