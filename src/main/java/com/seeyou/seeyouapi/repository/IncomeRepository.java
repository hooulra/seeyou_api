package com.seeyou.seeyouapi.repository;

import com.seeyou.seeyouapi.entity.Income;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IncomeRepository extends JpaRepository<Income, Long> {
}
