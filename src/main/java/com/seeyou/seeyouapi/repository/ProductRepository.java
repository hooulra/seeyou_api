package com.seeyou.seeyouapi.repository;

import com.seeyou.seeyouapi.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
}
