package com.seeyou.seeyouapi.repository;

import com.seeyou.seeyouapi.entity.Sell;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SellRepository extends JpaRepository<Sell, Long> {
    Page<Sell> findAllByIdGreaterThanEqualOrderByDateSellDesc (long id, Pageable pageable);

    List<Sell> findAllByBillNumber (long billNumber);
}
