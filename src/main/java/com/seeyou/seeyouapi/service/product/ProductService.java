package com.seeyou.seeyouapi.service.product;

import com.seeyou.seeyouapi.entity.Product;
import com.seeyou.seeyouapi.exception.CAccessDeniedException;
import com.seeyou.seeyouapi.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;

    public Product getProductData(long id) {
        return productRepository.findById(id).orElseThrow(CAccessDeniedException::new);
    }
}
